<?php
$host = 'localhost';
$username = 'root';
$password = '';
$dbname = 'CSE455';

$dsn = 'mysql:host='. $host .';dbname='. $dbname;

$Currency_name = $_GET['Euro'];
$rate = $_GET['rate'];
try {
  $pdo = new PDO($dsn, $username, $password);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  $sql = 'SELECT * FROM Currency WHERE Currency_name=?';
  $stmt = $pdo->prepare($sql);
  $stmt->execute([$Currency_name]);
  $posts = $stmt->fetch(PDO::FETCH_OBJ);
  
  $conObj->conversion = $posts->Currency_rate * $rate;
  echo json_encode($conObj);
}
catch(PDOException $e){
  echo "Connection failed: " . $e->getMessage();
}
?>